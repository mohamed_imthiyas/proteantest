﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeveloperTest.Migrations
{
    public partial class CustomerJobMap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateIndex(
               name: "Indx_Jobs_CustomerId",
               table: "Jobs",
               column: "CustomerId");

            migrationBuilder.AddForeignKey(
               name: "FK_Jobs_Customers_CustId",
               table: "Jobs",
               column: "CustomerId",
               principalTable: "Customers",
               principalColumn: "CustomerId",
               onDelete: ReferentialAction.Restrict);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                 name: "FK_Jobs_Customers_CustId",
                 table: "Jobs");

            migrationBuilder.DropIndex(
                name: "Indx_Jobs_CustomerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Jobs");
        }
    }
}
